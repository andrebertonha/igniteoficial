import { createContext, useContext, useState } from 'react';

const CyclesContext = createContext({} as any)

function NewCycleForm() {
    const { activeCycle, setActiveCycle } = useContext(CyclesContext)

    return (
        <div>
            <h1>new cycle form: {activeCycle}</h1>
            <button onClick={() => { setActiveCycle(2) }}>alterar ciclo ativo</button>
        </div>
        
    )
}

function Countdown () {
    const { activeCycle } = useContext(CyclesContext)

    return (
        <h1>countdown: {activeCycle} </h1>
    )
}

export function Home () {
    const [activeCycle, setActiveCycle] = useState(0)

    return (
        <CyclesContext.Provider value={{activeCycle, setActiveCycle}}>
            <div>
                <Countdown />
                <NewCycleForm />
            </div>
        </CyclesContext.Provider>
        
    )
}